//
// Created by Eelco on 12/29/2017.
//
#pragma once

#ifndef TEST_SCRIPT_H
#define TEST_SCRIPT_H


#include <vector>
#include <lua.hpp>
#include "ScriptComponent.h"

class Script {
    public:
        Script();
		~Script();
        void add( ScriptComponent * component );
        void init();
        void update(GameObject obj);

        static void stackDump( lua_State * state, const char * pre = "" );
    private:
        void registerFunctions();
        static int move( lua_State * state );
        static int pos( lua_State * state );


        std::vector< ScriptComponent *> components;
        lua_State * state;

};


#endif //TEST_SCRIPT_H
