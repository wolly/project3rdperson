//
// Created by Eelco on 12/29/2017.
//
#include "stdafx.h"
#include <iostream>
#include <cstdio>
#include "Script.h"
#include "ScriptComponent.h"

ScriptComponent::ScriptComponent( GameObject *parent, std::string type  )
:   parent( parent ), type( type ), thread( nullptr ) {
}

ScriptComponent::~ScriptComponent()
{
}

void ScriptComponent::init( lua_State * state ) {
    // check availablity of type lua table in global, if not warning
    lua_getglobal( state, "init" );
    if( lua_isfunction( state, -1 ) ) { // coroutines main function check
        lua_pushlightuserdata( state, parent );
        lua_getglobal( state, type.c_str() ); // routine
        if( lua_isfunction( state, -1 ) ) {
            if( lua_pcall( state, 2, 0, 0 ) != 0 ) {
                printf("Could not find %s( object, dt ) : %s\n", type.c_str(), lua_tostring(state, -1));
            }
        }
    } else {
        printf( "Could not find init( object, routine ) function\n" );
    }
    lua_settop( state, 0 ); //clear table from stack
}

void ScriptComponent::update( lua_State * state, float dt ) {
    lua_getglobal(state, "update");
    if (lua_isfunction(state, -1)) {
        lua_pushlightuserdata(state, parent);
        lua_pushnumber(state, dt);
        if (lua_pcall(state, 2, 0, 0) != 0) {
            printf("Lua error for update: %s", lua_tostring(state, -1));
        }
    }
}