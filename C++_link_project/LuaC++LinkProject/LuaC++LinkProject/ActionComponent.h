#pragma once
#include "Component.h"
class GameObject;
class ActionComponent :
	public Component
{
public:
	ActionComponent(GameObject* target);
	~ActionComponent();
protected: 
	GameObject* _obj;
};

