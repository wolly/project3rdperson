#pragma once
#include "Graphics.h"
#include "Script.h"
#include "Graphics.h"
#include "stdafx.h"

class Game {
public:
	Game();
	~Game();
	void init();
	void run();


private:
	std::vector<GameObject*> objects;
	Graphics graphics;
	Script script;

};

