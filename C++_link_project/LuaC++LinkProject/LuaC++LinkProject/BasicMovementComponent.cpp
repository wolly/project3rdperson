#include "stdafx.h"
#include "BasicMovementComponent.h"
#include "KeyHandler.h"
#include <iostream>

BasicMovementComponent::BasicMovementComponent(GameObject* target,std::list<std::string> keys,float speed):KeyComponent(keys),_speed(speed),_target(target)
{
	_type = 2;
}

BasicMovementComponent::~BasicMovementComponent()
{

}

void BasicMovementComponent::Update()
{
	std::list<sf::Keyboard::Key>::iterator it = _keys.begin();

	if (KeyHandler::KeyDown(it._Ptr->_Myval))
	{
		//std::cout << "the first key was pressed" << std::endl;
		_target->AddForce(-_speed, 0);
	}

	++it;

	if (KeyHandler::KeyDown(it._Ptr->_Myval))
	{
		//std::cout << "the second key was pressed" << std::endl;
		_target->AddForce(_speed, 0);
	}

	++it;

	if (KeyHandler::KeyDown(it._Ptr->_Myval))
	{
		//std::cout << "the third key was pressed" << std::endl;
		_target->AddForce(0, -_speed);
	}

	++it;

	if (KeyHandler::KeyDown(it._Ptr->_Myval))
	{
		//std::cout << "the fourth key was pressed" << std::endl;
		_target->AddForce(0, +_speed);
	}
}
