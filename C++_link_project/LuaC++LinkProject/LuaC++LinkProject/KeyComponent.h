#pragma once
#include "Component.h"
#include <SFML/Graphics.hpp>
#include <SFML/System.hpp>
#include <list>

class KeyComponent : public Component
{
public:
	KeyComponent();
	KeyComponent(std::list<std::string> _keys);
	virtual ~KeyComponent();
	virtual void Update();

protected:
	std::list<sf::Keyboard::Key> _keys;
};

