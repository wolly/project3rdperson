//
// Created by Eelco on 12/29/2017.
//
#pragma once

#include <SFML/Graphics.hpp>
#include "GameObject.h"

class GraphicsComponent {
    public:
        GraphicsComponent( GameObject * parent, float radius, sf::Color color );
		~GraphicsComponent();
        sf::Shape const & Shape(); // cannot change shape

        void draw( sf::RenderWindow & window );

        GameObject * Parent();
    private:
        GameObject * parent;
        sf::CircleShape shape;
};
