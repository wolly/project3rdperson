#pragma once
#include "ActionComponent.h"
class GameObject;
class FollowComponent : public ActionComponent
{
public:
	void Update();
	FollowComponent(GameObject* obj,GameObject* target,float speed);
	~FollowComponent();
private:
	float _speed;
	GameObject* _target;
};

