#pragma once
#include "KeyComponent.h"
#include "GameObject.h"
#include <list>
class BasicMovementComponent : public KeyComponent
{
public:
	BasicMovementComponent(GameObject* target,std::list<std::string> _keys,float speed);
	~BasicMovementComponent();
	void Update();
private:
	float _speed;
	GameObject* _target;
	sf::Vector2f _velocity;
};

