#pragma once
#include "stdafx.h"
#include <iostream>
#include <list>
#include <lua.hpp>
#include "PhysicsManager.h"
#include "Game.h"
class NewGame
{
public:
	NewGame();
	~NewGame();
	void Initialize();
	void Update();
	static void stackDump(lua_State * state, const char * pre = "");
private:
	static int PauseUnpause(lua_State * state);
	static int NewUnclassified(lua_State * state);
	static int NewPlayer(lua_State * state);
	static int NewEnemy(lua_State * state);
	static int NewBox(lua_State * state);
	static int Move(lua_State * state);
	static int CollidesWith(lua_State * state);
	static int KeyDown(lua_State * state);
	static int KeyUp(lua_State * state);
	static int KeyPressed(lua_State * state);
	static int ShowMessage(lua_State * state);
	static int RegisterKeyMovementComponent(lua_State * state);
	static int RegisterFollowComponent(lua_State * state);
	static int RegisterForCollisions(lua_State * state);
	void registerFunctions();
	void GameLoop();
	sf::RenderWindow* window;
	lua_State * state;
	sf::Font* font;
	static bool _paused;
	static std::string message;
	static int time;
	static sf::Text text;
	static std::list<GameObject*>* objects;
};

