//
// Created by Eelco on 12/29/2017.
//

#include "stdafx.h"
#include "GameObject.h"
#include "Graphics.h"
#include "PhysicsManager.h"
#include "BasicMovementComponent.h"
#include <SFML/Graphics.hpp>
#include <SFML/System.hpp>
#include <fstream>
#include "LineSegment.h"
#include "FollowComponent.h"
#include <iostream>
#include <algorithm>

GameObject::GameObject(std::string name, float x, float y, float radius, std::string type, GameObject * parent)
:   _name( name ), _x( x ), _y( y ), _radius(radius), _parent( parent ),_type(type)
{	
	_position.x = x;
	_position.y = y;
	_image = sf::CircleShape(radius);
	_image.setPosition(_x, _y);

	//PhysicsManager::RegisterLine(new LineSegment(&_actualPosition,&_nextPosition));
	float rad = _image.getRadius();
	_actualPosition = _position;
	_actualPosition.x += rad;
	_actualPosition.y += rad;

	_nextPosition = _actualPosition + _velocity + PhysicsManager::CheapNormalize(_velocity, 3)*_radius;
	//_texture.loadFromFile("Hero.png");
	
	//_image.setTexture(_texture);
	//_image.setPosition(sf::Vector2f(10, 50));
	
}

GameObject::~GameObject()
{
}

void GameObject::KeyActions()
{
	for each (KeyComponent* comp in _keyActions)
	{
		comp->Update();
	}
}

void GameObject::Update()
{
	float rad = _image.getRadius();
	_actualPosition = _position;
	_actualPosition.x += rad;
	_actualPosition.y += rad;

	_nextPosition = _actualPosition + _velocity+PhysicsManager::CheapNormalize(_velocity,3)*_radius;
	KeyActions();

	for each (ActionComponent* comp in _updateActions)
	{
		comp->Update();
	}

	Move(_velocity);

}

void GameObject::StartsCollisionsWith(GameObject * obj)
{
	_nameCols.insert(obj);
	//_typeCols.push_back(obj->GetType());
}

void GameObject::StopsCollisionsWith(GameObject * obj)
{
	_nameCols.erase(obj);
	if (std::find(_typeCols.begin(), _typeCols.end(), obj->GetType()) != _typeCols.end())
	{
		std::cout << "calls remove on "<< _name << std::endl;

		_typeCols.remove(obj->GetType());
	}
}

bool GameObject::CollidesWith(std::string typeorname)
{
	std::set<GameObject*>::iterator it;
	for (it = _nameCols.begin(); it != _nameCols.end(); ++it)
	{
		if (it._Ptr->_Myval->GetType()==typeorname || it._Ptr->_Myval->GetName() == typeorname)
			return true;
	}
	/*if (_nameCols.find(typeorname) != _nameCols.end())
	{
		return true;
	}
	else if(std::find(_typeCols.begin(), _typeCols.end(), typeorname) != _typeCols.end())//list has weird removes however set works perfectly
	{
		return true;
	}*/

	
	return false;
	std::cout << "doesnt collide" << std::endl;
	
}

void GameObject::move( float mx, float my ) {
    _position.x += mx;
    _position.y += my;
	float rad = _image.getRadius();
	_actualPosition = _position;
	_actualPosition.x += rad;
	_actualPosition.y += rad;

	_nextPosition = _actualPosition + _velocity + PhysicsManager::CheapNormalize(_velocity, 3)*_radius;
	_image.setPosition(_x, _y);
}

sf::CircleShape GameObject::GetSprite()
{
	return _image;
}

std::string GameObject::GetName() {
    return _name;
}

sf::Vector2f GameObject::GetPosition()
{
	return _position;
}
sf::Vector2f GameObject::GetActualPosition()
{
	return _actualPosition;
}
void GameObject::SetPosition(sf::Vector2f vec)
{
	_position = vec;
	_position.x -= _radius;
	_position.y -= _radius;
	float rad = _image.getRadius();
	_actualPosition = _position;
	_actualPosition.x += rad;
	_actualPosition.y += rad;

	_nextPosition = _actualPosition + _velocity + PhysicsManager::CheapNormalize(_velocity, 3)*_radius;
	_image.setPosition(_position);
}
LineSegment GameObject::GetSegment()
{
	return LineSegment(&_actualPosition,&_nextPosition);
}
void GameObject::SetPosition(float tX,float tY)
{
	_position.x = tX;
	_position.y = tY;
	float rad = _image.getRadius();
	_actualPosition = _position;
	_actualPosition.x += rad;
	_actualPosition.y += rad;

	_nextPosition = _actualPosition + _velocity + PhysicsManager::CheapNormalize(_velocity, 3)*_radius;
	_image.setPosition(_position);
}

void GameObject::Move(sf::Vector2f vec)
{
	_position += vec;
	_image.setPosition(_position);
}

void GameObject::Move(float tX, float tY)
{
	_position.x += tX;
	_position.y += tY;
	float rad = _image.getRadius();
	_actualPosition = _position;
	_actualPosition.x += rad;
	_actualPosition.y += rad;

	_nextPosition = _actualPosition + _velocity + PhysicsManager::CheapNormalize(_velocity, 3)*_radius;
	_image.setPosition(_position);
}

void GameObject::RegisterComponent(Component* comp)
{
	if (comp->GetType()==2) {
		std::cout << "registered key component" << std::endl;
		_keyActions.push_back(static_cast<BasicMovementComponent*>(comp));
	}
	else if(comp->GetType() == 3)
	{
		std::cout << "registered follow component" << std::endl;
		_updateActions.push_back(static_cast<FollowComponent*>(comp));
	}
}

float GameObject::x() {
    return _x;
}
float GameObject::y() {
    return _y;
}

sf::Vector2f GameObject::GetVelocity()
{
	return _velocity;
}

void GameObject::SetVelocity(sf::Vector2f vec)
{
	_velocity = vec;
}

void GameObject::SetVelocity(float tX, float tY)
{
	_velocity.x = tX;
	_velocity.y = tY;
}

void GameObject::AddForce(sf::Vector2f vec)
{
	_velocity += vec;
}

void GameObject::AddForce(float tX, float tY)
{
	_velocity.x += tX;
	_velocity.y += tY;
}

void GameObject::SetColor(sf::Color color)
{
	_image.setFillColor(color);
}

std::string GameObject::GetType()
{
	return _type;
}
