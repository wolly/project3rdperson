#include "stdafx.h"
#include "FollowComponent.h"
#include "PhysicsManager.h"
#include "GameObject.h"


void FollowComponent::Update()
{
	_obj->SetVelocity(PhysicsManager::CheapNormalize((_target->GetActualPosition() - _obj->GetActualPosition()), 20)*_speed);
}

FollowComponent::FollowComponent(GameObject* obj,GameObject* target,float speed):ActionComponent(obj),_target(target),_speed(speed)
{
	_type = 3;
	std::cout <<"target name = "<< target->GetName()<<std::endl;
}


FollowComponent::~FollowComponent()
{
}
