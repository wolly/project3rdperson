//
// Created by Eelco on 12/29/2017.
//

#include "stdafx.h"
#include <iostream>
#include "Script.h"



Script::Script( ) {
    state = luaL_newstate();
    luaL_openlibs( state );
    //printf("SA" );

    luaL_dofile( state, "main.lua" );

    //printf("SB" );
    registerFunctions();
}

Script::~Script()
{
}

void Script::init() {
    for( auto i = components.begin(); i < components.end(); ++i ) {
        ScriptComponent * component = *i;
        component->init( state );
    }
}

void Script::update(GameObject obj) {
    //printf("Script update\n");
    for( auto i = components.begin(); i < components.end(); ++i ) {
        ScriptComponent * component = *i;
        component->update( state);
    }
}

void Script::add(ScriptComponent *component) {
    components.push_back( component );
}



void Script::registerFunctions() {
    lua_newtable( state );
        lua_pushcfunction( state, Script::move );
        lua_setfield( state, -2, "move" );
        lua_pushcfunction( state, Script::pos );
        lua_setfield( state, -2, "pos" );
    lua_setglobal( state, "Object" );
	std::cout << "registered functions" << std::endl;
}

int Script::move( lua_State * state ) {
    if (lua_gettop(state) == 3 && lua_isuserdata(state, 1) && lua_isnumber(state, 2) && lua_isnumber(state, 3)) {
		std::cout << "movoing" << std::endl;
        GameObject * object = (GameObject *) lua_topointer( state, 1 );
        float mx = (float) lua_tonumber( state, 2 );
        float my = (float) lua_tonumber( state, 3 );
        object->move( mx, my );
        return lua_yield( state, 0 );
    }
    return luaL_error( state, "Object.move( mx, my ), faulty arguments" );
}

int Script::pos( lua_State * state ) {
    if (lua_gettop(state) == 1 && lua_isuserdata( state, 1) ) {
        GameObject * object = (GameObject *) lua_topointer( state, 1 );
        float x = object->x();
        float y = object->y();
        //lua_settop( state, 0 ); // pop object
        lua_pushnumber( state, x );
        lua_pushnumber( state, y );
        return 2;
    }
    return luaL_error( state, "Object.move( mx, my ), faulty arguments" );
}


void Script::stackDump (lua_State *state, const char * pre ) {
    printf("Lua Stack (%s)> ", pre );
    int i;
    int top = lua_gettop(state);
    for (i = 1; i <= top; i++) {  /* repeat for each level */
        int t = lua_type(state, i);
        switch (t) {

            case LUA_TSTRING:  /* strings */
                printf("(%d)%s'", i, lua_tostring(state, i));
                break;

            case LUA_TBOOLEAN:  /* booleans */
                printf("(%d)%s", i, lua_toboolean(state, i) ? "true" : "false");
                break;

            case LUA_TNUMBER:  /* numbers */
                printf("(%d)%g", i, lua_tonumber(state, i));
                break;

            default:  /* other values */
                printf("(%d)%s", i, lua_typename(state, t));
                break;

        }
        printf("  ");  /* put a separator */
    }
    printf("\n");  /* end the listing */
}

