#include "stdafx.h"
#include "KeyComponent.h"
#include "KeyHandler.h"



KeyComponent::KeyComponent()
{

}

KeyComponent::KeyComponent(std::list<std::string> keys)
{
	_type = 1;
	for (std::list<std::string>::iterator it = keys.begin(); it != keys.end(); ++it)
	{
		_keys.push_back(KeyHandler::ConvertIntoKey((it._Ptr->_Myval)));
	}
}


KeyComponent::~KeyComponent()
{
}

void KeyComponent::Update()
{
}

