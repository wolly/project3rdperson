//
// Created by Eelco on 12/29/2017.
//

#ifndef TEST_SCRIPTCOMPONENT_H
#define TEST_SCRIPTCOMPONENT_H


#include <lua.hpp>
#include "GameObject.h"

class ScriptComponent {
    public:
        ScriptComponent( GameObject * parent, std::string type );
		~ScriptComponent();
        void init( lua_State * state );
        void update( lua_State * state, float dt = 1.0f/60.0f );
    private:
        GameObject * parent;
        std::string type;
//        bool typeExists;
//        bool initExists;
//        bool updateExists;

        lua_State * thread;
};


#endif //TEST_SCRIPTCOMPONENT_H
