#include "stdafx.h"
#include "NewGame.h"
#include <iostream>
#include <list>
#include <lua.hpp>
#include "lauxlib.h"
#include "lualib.h"
#include "Game.h"
#include "stdafx.h"
#include "Graphics.h"
#include "KeyHandler.h"
#include "BasicMovementComponent.h"
#include "FollowComponent.h"


std::list<GameObject*>* NewGame::objects = new std::list<GameObject *>();
int NewGame::time = 0;
sf::Text NewGame::text = sf::Text();
std::string NewGame::message = "";
bool NewGame::_paused = false;

NewGame::NewGame()
{
	text.setString("");
	text.setColor(sf::Color::White);
	time = 0; 
	//font = new sf::Font();
	//if (!font->loadFromFile("OpenSans-Bold.ttf"))
	//{
	//	// Error...
	//	std::cout << "error loading font";
	//}
	state = luaL_newstate();
	luaL_openlibs(state);
	window = new sf::RenderWindow(sf::VideoMode(1000, 1000), "Lua Game");

	registerFunctions();
	luaL_dofile(state, "main.lua");
	Initialize();
}

void NewGame::registerFunctions() {
	lua_newtable(state);
	lua_pushcfunction(state, NewPlayer);
	lua_setfield(state, -2, "NewPlayer");
	lua_pushcfunction(state, NewEnemy);
	lua_setfield(state, -2, "NewEnemy");
	lua_pushcfunction(state, NewUnclassified);
	lua_setfield(state, -2, "NewUnclassified");
	lua_pushcfunction(state, NewBox);
	lua_setfield(state, -2, "NewBox");
	lua_pushcfunction(state, CollidesWith);
	lua_setfield(state, -2, "CollidesWith");
	lua_pushcfunction(state, Move);
	lua_setfield(state, -2, "Move");
	lua_pushcfunction(state, KeyDown);
	lua_setfield(state, -2, "KeyDown");
	lua_pushcfunction(state, KeyUp);
	lua_setfield(state, -2, "KeyUp");
	lua_pushcfunction(state, KeyPressed);
	lua_setfield(state, -2, "KeyPressed");
	lua_pushcfunction(state, RegisterKeyMovementComponent);
	lua_setfield(state, -2, "AddKeyMovementComponent");
	lua_pushcfunction(state, RegisterFollowComponent);
	lua_setfield(state, -2, "AddFollowComponent");
	lua_pushcfunction(state, RegisterForCollisions);
	lua_setfield(state, -2, "RegisterForCollisions");
	lua_pushcfunction(state, ShowMessage);
	lua_setfield(state, -2, "ShowMessage");
	lua_pushcfunction(state, PauseUnpause);
	lua_setfield(state, -2, "PauseUnpause");
	lua_setglobal(state, "Game");

	std::cout << "registered functions" << std::endl;
}
int NewGame::NewUnclassified(lua_State * state)
{
	if (lua_gettop(state) == 5 && lua_isstring(state, 1) && lua_isnumber(state, 2) && lua_isnumber(state, 3) && lua_isnumber(state, 4) && lua_isstring(state, 5)) {

		GameObject* obj1 = new GameObject((std::string)lua_tostring(state, 1), (float)lua_tonumber(state, 2), (float)lua_tonumber(state, 3), (float)lua_tonumber(state, 4), (std::string)lua_tostring(state, 5));
		obj1->SetColor(sf::Color::Yellow);
		objects->push_back(obj1);

		lua_pushlightuserdata(state, obj1);

		return 1;
	}

	return luaL_error(state, "Game.NewTalkable(name,x,y,type), faulty arguments");
}

void NewGame::GameLoop()
{
	PhysicsManager::ShowDebug(true);

	//PhysicsManager::RegisterLine(new LineSegment(new sf::Vector2f(100, 500),new sf::Vector2f(300, 500)));

	window->setVerticalSyncEnabled(true);
	window->setFramerateLimit(60); while (window->isOpen())
	while(window->isOpen())
	{
		// clear the window with black color
		window->clear(sf::Color::Black);

			sf::Event event;
			if (window->pollEvent(event))
			{
				if (event.type == sf::Event::Closed)
					window->close();
				if (event.type == sf::Event::KeyPressed)
				{
					if (event.key.code == sf::Keyboard::Escape)
					{
						std::cout << "the escape key was pressed" << std::endl;
						window->close();
					}
				}
				KeyHandler::NewEvent(event);
			}

			Update();

			if (!_paused)
			{
				//_circle1.UpdateMovement(window, speed);
				/*
						for (std::list<GameObject*>::iterator it = objects->begin(); it != objects->end(); ++it)
						{
							(it._Ptr->_Myval)->KeyActions(&event);
						}*/
				for (std::list<GameObject*>::iterator it = objects->begin(); it != objects->end(); ++it)
				{
					(it._Ptr->_Myval)->Update();
				}

				//physics here
				PhysicsManager::Update(window);

			}
		for (std::list<GameObject*>::iterator it = objects->begin(); it != objects->end(); ++it)
		{
			window->draw(it._Ptr->_Myval->GetSprite());
			/*
						sf::Vertex line[] =
						{
							it._Ptr->_Myval->GetActualPosition(),
							it._Ptr->_Myval->GetNextPosition()

						};

						window->draw(line, 2, sf::Lines);*/

		}
		// check all the window's events that were triggered since the last iteration of the loop
		/*
		for( unsigned int i = 0; i < objects.size(); i++ ) {
		window.draw(objects[i]);
		}*/
		if (message.length() > 0)//sfml text showing did not work for me, i tryed to get it to work multiple ways but still gave me an acces violation so i had to resort to this
		{
			std::cout << message;
			message = "";
		}
		//window->draw(text);
		// end the current frame
		window->display();
		/*
		sf::Event event;
		if (window.pollEvent(event))
		{

		}

		window.display();*/
	}
}

int NewGame::PauseUnpause(lua_State * state)
{
	if (lua_gettop(state) == 1, lua_isboolean(state, 1))
	{
		_paused = lua_toboolean(state, 1);
		return 0;
	}
	return luaL_error(state, "Game.PauseUnpause(pauseState), faulty arguments");
}


int NewGame::NewPlayer(lua_State * state)
{
	if (lua_gettop(state) == 9 && lua_isstring(state, 1) && lua_isnumber(state, 2) && lua_isnumber(state, 3) && lua_isnumber(state, 4) 
		&& lua_isstring(state, 5) && lua_isstring(state, 6) && lua_isstring(state, 7) && lua_isstring(state, 8) && lua_isnumber(state, 9)) {
		GameObject* obj1 = new GameObject((std::string)lua_tostring(state,1), (float)lua_tonumber(state,2), (float)lua_tonumber(state, 3), (float)lua_tonumber(state, 4),"player");
		obj1->SetColor(sf::Color::Green);

		std::string left = (std::string)lua_tostring(state, 5);
		std::string right = (std::string)lua_tostring(state, 6);
		std::string up = (std::string)lua_tostring(state, 7);
		std::string down = (std::string)lua_tostring(state, 8);
		std::list<std::string> keys;
		keys.push_back(left);
		keys.push_back(right);
		keys.push_back(up);
		keys.push_back(down);
		obj1->RegisterComponent(new BasicMovementComponent(obj1, keys, (float)lua_tonumber(state, 9)));

		objects->push_back(obj1);

		lua_pushlightuserdata(state, obj1); 

		return 1;
	}
	
	return luaL_error(state, "Game.NewPlayer(name, x,y,radius,left,right,up,down,speed), faulty arguments");

}

int NewGame::NewEnemy(lua_State * state)
{
	if (lua_gettop(state) == 6 && lua_isstring(state, 1) && lua_isnumber(state, 2) && lua_isnumber(state, 3) && lua_isnumber(state, 4) && lua_isuserdata(state, 5) && lua_isnumber(state, 6)) {
		GameObject* obj1 = new GameObject((std::string)lua_tostring(state, 1), (float)lua_tonumber(state, 2), (float)lua_tonumber(state, 3), (float)lua_tonumber(state, 4),"enemy");
		obj1->SetColor(sf::Color::Red);
		obj1->RegisterComponent(new FollowComponent(obj1, (GameObject*)lua_topointer(state, 5),(float)lua_tonumber(state, 6)));
		objects->push_back(obj1);

		lua_pushlightuserdata(state, obj1);

		return 1;
	}

	return luaL_error(state, "Game.NewEnemy(name,x,y,target,speed), faulty arguments");
}

int NewGame::NewBox(lua_State * state)
{
	if (lua_gettop(state) == 4 && lua_isnumber(state, 1) && lua_isnumber(state, 2) && lua_isnumber(state, 3) && lua_isnumber(state, 4)) {
		float x = (float)lua_tonumber(state, 1);
		float y = (float)lua_tonumber(state, 2);
		float widht = (float)lua_tonumber(state, 3);
		float height = (float)lua_tonumber(state, 4);

		sf::RectangleShape rectangle(sf::Vector2f(widht, height));
		rectangle.setPosition(sf::Vector2f(x-widht/2,y-height/2));
		PhysicsManager::RegisterUnMovable(rectangle,widht,height);

		//lua_pushlightuserdata(state, obj1);

		return 1;
	}

	return luaL_error(state, "Game.NewBox(x,y,widht,height), faulty arguments");
}


int NewGame::Move(lua_State * state) {
	if (lua_gettop(state) == 3 && lua_isuserdata(state, 1) && lua_isnumber(state, 2) && lua_isnumber(state, 3)) {
		//std::cout << "movoing";
		GameObject * object = (GameObject *)lua_topointer(state, 1);
		float mx = (float)lua_tonumber(state, 2);
		float my = (float)lua_tonumber(state, 3);
		object->Move(mx, my);
		return 0;
	}
	return luaL_error(state, "Object.move( mx, my ), faulty arguments");
}

int NewGame::CollidesWith(lua_State * state)
{
	if (lua_gettop(state) == 2 && lua_isuserdata(state, 1) && lua_isstring(state, 2)) {
		//std::cout << "movoing";
		GameObject * object = (GameObject *)lua_topointer(state, 1);
		lua_pushboolean(state,object->CollidesWith((std::string)lua_tostring(state,2)));
		return 1;
	}
	return luaL_error(state, "Object.CollidesWith( obj, name or type ), faulty arguments");
}

int NewGame::KeyDown(lua_State * state)
{
	if (lua_gettop(state) == 1&& lua_isstring(state, 1)) {
		//std::cout << "movoing";
		lua_pushboolean(state, KeyHandler::KeyDown((std::string)lua_tostring(state, 1)));
		return 1;
	}
	return luaL_error(state, "Object.KeyDown( key ), faulty arguments");
}

int NewGame::KeyUp(lua_State * state)
{
	if (lua_gettop(state) == 1 && lua_isstring(state, 1)) {
		//std::cout << "movoing";
		lua_pushboolean(state, KeyHandler::KeyUp((std::string)lua_tostring(state, 1)));
		return 1;
	}
	return luaL_error(state, "Object.KeyDown( key ), faulty arguments");
}

int NewGame::KeyPressed(lua_State * state)
{
	if (lua_gettop(state) == 1 && lua_isstring(state, 1)) {
		//std::cout << "movoing";
		lua_pushboolean(state, KeyHandler::KeyPressed((std::string)lua_tostring(state, 1)));
		return 1;
	}
	return luaL_error(state, "Object.KeyDown( key ), faulty arguments");
}

int NewGame::ShowMessage(lua_State * state)
{
	if (lua_gettop(state) == 4 && lua_isstring(state, 1) && lua_isnumber(state, 2) && lua_isnumber(state, 3) && lua_isnumber(state, 4)/* && lua_isnumber(state, 5)*/) {

		//text.setString("start message");
		message = (std::string)lua_tostring(state, 1);
		message += "\n";
		text.setString((sf::String)lua_tostring(state,1));
		text.setCharacterSize((int)lua_tonumber(state, 4));
		text.setPosition(lua_tonumber(state, 2), lua_tonumber(state, 3));
		return 0;
	}
	return luaL_error(state, "Object.ShowMessage( string,x,y, size), faulty arguments");
}

int NewGame::RegisterKeyMovementComponent(lua_State * state)
{
	if (lua_gettop(state) == 6 && lua_isuserdata(state, 1) && lua_isstring(state, 2) && lua_isstring(state, 3) && lua_isstring(state, 4) && lua_isstring(state, 5) && lua_isnumber(state, 6)) {
			//std::cout << "movoing";
			GameObject * object = (GameObject *)lua_topointer(state, 1);
			std::string left = (std::string)lua_tostring(state, 2);
			std::string right = (std::string)lua_tostring(state, 3);
			std::string up = (std::string)lua_tostring(state, 4);
			std::string down = (std::string)lua_tostring(state, 5);
			std::list<std::string> keys;
			keys.push_back(left);
			keys.push_back(right);
			keys.push_back(up);
			keys.push_back(down);
			object->RegisterComponent(new BasicMovementComponent(object, keys, (float)lua_tonumber(state, 6)));
			return 0;
	}
	return luaL_error(state, "Object.RegisterKeyMovementComponent( object , leftKey , rightKey , upKey , downKey , speed), faulty arguments");
}

int NewGame::RegisterFollowComponent(lua_State * state)
{
	if (lua_gettop(state) == 3 && lua_isuserdata(state, 1) && lua_isuserdata(state, 2) && lua_isnumber(state, 3)) {
		//std::cout << "movoing";
		GameObject * object = (GameObject *)lua_topointer(state, 1);

		object->RegisterComponent(new FollowComponent(object, (GameObject *)lua_topointer(state,2),lua_tonumber(state,3)));
		return 0;
	}
	return luaL_error(state, "Object.RegisterKeyMovementComponent( object , target , speed), faulty arguments");
}

int NewGame::RegisterForCollisions(lua_State * state)
{
	if (lua_gettop(state) == 1 && lua_isuserdata(state, 1)) {
		//std::cout << "movoing";
		GameObject * object = (GameObject *)lua_topointer(state, 1);
		PhysicsManager::RegisterObject(object);
		return 0;
	}
	return luaL_error(state, "Object.RegisterForCollisions( object ), faulty arguments");
}

NewGame::~NewGame()
{
}


void NewGame::Initialize()
{
	std::cout << "initializing"<<std::endl;
	lua_getglobal(state, "Initialize"); // pushes global update
	int isFunc = lua_isfunction(state, -1);
	if (isFunc) {
		//lua_pushlightuserdata(state, &sprite);
		if (lua_pcall(state, 0, 0, 0) != 0) {
			//printf("Error %s\n", lua_tostring(state, -1));
			//exit(-1);
		}
	}
	GameLoop();
}

void NewGame::Update()
{
	lua_getglobal(state, "Update"); // pushes global update
	if (lua_isfunction(state, -1)) {
		if (lua_pcall(state, 0, 0, 0) != 0) {
			//printf("Error %s\n", lua_tostring(state, -1));
		}
	}

	if (KeyHandler::KeyDown("a"))
		std::cout << "down";

	if (KeyHandler::KeyPressed("a"))
		std::cout << "pressed";

	if (KeyHandler::KeyUp("a"))
		std::cout << "up";
}


void NewGame::stackDump(lua_State *state, const char * pre) {
	//printf("Lua Stack (%s)> ", pre);
	int i;
	int top = lua_gettop(state);
	for (i = 1; i <= top; i++) {  /* repeat for each level */
		int t = lua_type(state, i);
		switch (t) {

		case LUA_TSTRING:  /* strings */
			//printf("(%d)%s'", i, lua_tostring(state, i));
			break;

		case LUA_TBOOLEAN:  /* booleans */
			//printf("(%d)%s", i, lua_toboolean(state, i) ? "true" : "false");
			break;

		case LUA_TNUMBER:  /* numbers */
			//printf("(%d)%g", i, lua_tonumber(state, i));
			break;

		default:  /* other values */
			//printf("(%d)%s", i, lua_typename(state, t));
			break;

		}
		//printf("  ");  /* put a separator */
	}
	//printf("\n");  /* end the listing */
}