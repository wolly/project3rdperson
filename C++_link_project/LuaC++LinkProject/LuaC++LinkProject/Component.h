#pragma once
class Component
{
public:
	Component();
	~Component();
	virtual void Update();
	int GetType();
protected:
	int _type;
};

