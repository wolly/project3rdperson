//
// Created by Eelco on 12/29/2017.
//

#include "stdafx.h"
#include "Graphics.h"

Graphics::Graphics()
: window( sf::VideoMode(800, 600), "SFML works!" )
{
    window.setVerticalSyncEnabled( true );
}

Graphics::~Graphics()
{
    window.close();
}

void Graphics::add(GraphicsComponent * component) {
    components.push_back( component );
}

void Graphics::init() {}

void Graphics::draw() {
    sf::Event event;
    while (window.pollEvent(event))
    {
        if (event.type == sf::Event::Closed)
            window.close();
    }

    window.clear();
 //   window.draw(shape);

        for( auto i = components.begin(); i < components.end(); ++i ) {
            GraphicsComponent * component = *i;
            component->draw( window );
        }
    window.display();
}

bool Graphics::isOpen() {
    return window.isOpen();
}




