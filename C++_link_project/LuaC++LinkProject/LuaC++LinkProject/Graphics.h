//
// Created by Eelco on 12/29/2017.
//

#pragma once
#include <vector> // std::vector
#include <SFML/Graphics.hpp>
#include "GraphicsComponent.h"

class Graphics {
public:
    Graphics();
    ~Graphics();

    void add( GraphicsComponent * component );
    void init();
    void draw();
    bool isOpen();

private:
    sf::RenderWindow window;

    std::vector<GraphicsComponent *> components;
};


