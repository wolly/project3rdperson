//
// Created by Eelco on 12/29/2017.
//
#include "stdafx.h"
#include "GraphicsComponent.h"


GraphicsComponent::GraphicsComponent( GameObject * parent, float radius, sf::Color color )
:   parent( parent ) {
    shape.setRadius( radius );
    shape.setFillColor( color );
}

GraphicsComponent::~GraphicsComponent()
{
}

sf::Shape const & GraphicsComponent::Shape() {
    return shape;
}

GameObject * GraphicsComponent::Parent() {
    return parent;
}

void GraphicsComponent::draw( sf::RenderWindow & window ) {
    shape.setPosition( parent->x(), parent->y() );
    window.draw( shape );
}

