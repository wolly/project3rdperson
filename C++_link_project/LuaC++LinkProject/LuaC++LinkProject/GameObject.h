//
// Created by Eelco on 12/29/2017.
//

#ifndef TEST_GAMEOBJECT_H
#define TEST_GAMEOBJECT_H

#pragma once
#include <string>
#include <SFML/Audio.hpp>
#include <SFML/Graphics.hpp>
#include  <list>
#include  <set>
#include "KeyComponent.h"
#include "LineSegment.h"
#include "Component.h"
#include "ActionComponent.h"

class GameObject {
public:
    GameObject( std::string name, float x, float y,float radius, std::string type, GameObject * parent = nullptr );
	~GameObject();

	void KeyActions();
	void Update();
	void StartsCollisionsWith(GameObject* obj);
	void StopsCollisionsWith(GameObject* obj);
	bool CollidesWith(std::string name);
    void move( float mx, float my );
	sf::CircleShape GetSprite();
    std::string GetName();
	sf::Vector2f GetPosition();
	sf::Vector2f GetActualPosition();
	void SetPosition(sf::Vector2f vec);
	LineSegment GetSegment();
	void SetPosition(float tX, float tY);
	void Move(sf::Vector2f vec);
	void Move(float tX, float tY);
	void RegisterComponent(Component* comp);
    float x();
    float y();
	sf::Vector2f GetVelocity();
	void SetVelocity(sf::Vector2f);
	void SetVelocity(float tX, float tY);
	void AddForce(sf::Vector2f);
	void AddForce(float tX,float tY);
	void SetColor(sf::Color color);
	std::string GetType();
private:
	std::set<GameObject*> _nameCols;
	std::list<std::string> _typeCols;
	sf::Vector2f _velocity;
	sf::Vector2f _actualPosition;
	sf::Vector2f _nextPosition;
	std::string _name;
	std::string _type;
	sf::Texture _texture;
	sf::CircleShape _image;
	sf::Vector2f _position;
    float _x;
    float _y;
	float _radius;
    GameObject * _parent;
	std::list<KeyComponent*> _keyActions;
	std::list<ActionComponent*> _updateActions;
};


#endif //TEST_GAMEOBJECT_H
